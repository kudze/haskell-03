module ExSet3 where
import Test.QuickCheck;

--1
type X = Float;
type Y = Float;
type Coords = (X, Y);

type Top = Float;
type Bottom = Float;
type Left = Float;
type Right = Float;
type RectangleBounds = (Top, Bottom, Left, Right)

type Radius = Float;
type Height = Float;
type Width = Float;
data Shape = Circle Radius Coords | Rectangle Height Width Coords
    deriving (Show, Ord, Eq)

dist :: Coords -> Coords -> Float 
dist (x1, y1) (x2, y2) = sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

rectangleBounds :: Shape -> RectangleBounds
rectangleBounds (Rectangle h w (x, y)) = (y + hh, y - hh, x - hw, x + hw)
    where
        hh = h / 2
        hw = w / 2
rectangleBounds (Circle _ _) = error "Only defined for rectangle!"

overlaps :: Shape -> Shape -> Bool
overlaps (Rectangle h1 w1 c1) (Rectangle h2 w2 c2) = 
    h1 /= 0 && w1 /= 0 && h2 /= 0 && w2 /= 0 && --we do not overlap if area = 0
    l1 < r2 && l2 < r1 && b1 < t2 && b2 < t1 --we check by coordinates of borders, since no rotation is allowed.
    where
        (t1, b1, l1, r1) = rectangleBounds (Rectangle h1 w1 c1)
        (t2, b2, l2, r2) = rectangleBounds (Rectangle h2 w2 c2)
        
overlaps (Circle r1 c1) (Circle r2 c2) = (max r1 r2) > (dist c1 c2)
overlaps (Rectangle h1 w1 c1) (Circle r2 (x2, y2)) = r2 > _dist
    where
        clamp :: Float -> Float -> Float -> Float
        clamp val _min _max = max _min (min _max val)
        (t1, b1, l1, r1) = rectangleBounds (Rectangle h1 w1 c1)
        closestPoint = (clamp x2 l1 r1, clamp y2 b1 t1)
        _dist = dist closestPoint (x2, y2)
        
overlaps (Circle r1 (x1, y1)) (Rectangle h2 w2 (x2, y2)) = overlaps (Rectangle h2 w2 (x2, y2)) (Circle r1 (x1, y1))

-- 2
any1 :: (a->Bool) -> [a] -> Bool
any1 cb l = length (filter cb l) /= 0

any2 :: (a->Bool) -> [a] -> Bool
any2 cb l = foldr (||) False (map cb l)

all1 :: (a->Bool) -> [a] -> Bool
all1 cb l = length (filter cb l) == length l

all2 :: (a->Bool) -> [a] -> Bool
all2 cb l = foldr (&&) True (map cb l)

prop_any :: [Int] -> Bool
prop_any l = any (>0) l == any1 (>0) l && any (>0) l == any2 (>0) l

prop_all :: [Int] -> Bool
prop_all l = all (>0) l == all1 (>0) l && all (>0) l == all2 (>0) l

-- 3
_unzip :: [(a, b)] -> ([a], [b])
_unzip s = foldr (\(a, b) (as, bs) -> (a:as, b:bs)) ([], []) s

-- 4
length1 :: [a] -> Int
length1 s = (sum . map (\x -> 1)) s

length2 :: [a] -> Int
length2 s = foldr (\s c -> c + 1) 0 s

prop_length :: [a] -> Bool
prop_length s = length s == length1 s && length s == length2 s

-- 5
ff :: Integer -> [Integer] -> Integer
ff _max = min _max . sum . map (10*) . filter (>=0)

-- 6
_total :: (Integer -> Integer) -> Integer -> Integer
_total fn n = (sum . map fn) [0, 1 .. n]

-- 7
iter1 :: Integer -> (a -> a) -> (a -> a)
iter1 1 f = f
iter1 n f
    | n <= 0 = error("n should be positive")
    | otherwise = f . iter1 (n-1) f

iter2 :: Int -> (a -> a) -> (a -> a)
iter2 n f 
    | n <= 0 = error("n should be positive!")
    | otherwise = foldr (.) f (replicate (n - 1) f)

-- map (iter1 6 (+1)) [1,2,3]

-- 8
splits :: [a] -> [([a],[a])]
splits s = map (\x -> x s) (map splitAt [0, 1 .. (length s)])